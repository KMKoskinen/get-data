<?php

namespace Drupal\get_data;

use Drupal\devel;
use Drupal\PHPExcel;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\SafeMarkup;

class Get_dataForm extends FormBase {
  protected $id;

  function getFormID() {
    return 'get_data_add';
  }

  function buildForm(array $form, FormStateInterface $form_state) {
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Fetch'),
    ); 
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    /*Nothing to validate on this form*/
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    module_load_include('inc', 'phpexcel');
    // file_put_contents('/sites/default/files/list.xlsx', file_get_contents('http://www.alko.fi/contentassets/df76c4146bb74ac08089e5bf610e6d67/fi/alkon-hinnasto-tekstitiedostona.xlsx'));	
    $data = phpexcel_import('sites/default/files/excel/alkon-hinnasto-tekstitiedostona.xls', FALSE);
    $date = REQUEST_TIME; 
    $data = $data[0];
    if (!empty($data)) {
	foreach ($data as $entry) {
		Get_dataStorage::add(SafeMarkup::checkPlain($entry[0]), 
					   SafeMarkup::checkPlain($entry[1]),
					   SafeMarkup::checkPlain($entry[2]),
					   SafeMarkup::checkPlain($entry[3]),
					   SafeMarkup::checkPlain($entry[4]),
					   SafeMarkup::checkPlain($entry[5]),
					   SafeMarkup::checkPlain($entry[6]),
					   SafeMarkup::checkPlain($entry[7]),
					   SafeMarkup::checkPlain($entry[8]),
					   SafeMarkup::checkPlain($entry[9]),
					   SafeMarkup::checkPlain($entry[10]),
					   SafeMarkup::checkPlain($entry[11]),
					   SafeMarkup::checkPlain($entry[12]),
					   SafeMarkup::checkPlain($entry[13]),
					   SafeMarkup::checkPlain($entry[15]),
					   SafeMarkup::checkPlain($entry[16]),
					   SafeMarkup::checkPlain($entry[17]),
					   SafeMarkup::checkPlain($entry[18]),
					   SafeMarkup::checkPlain($entry[19]),
					   SafeMarkup::checkPlain($entry[20]),
					   SafeMarkup::checkPlain($entry[21]),
					   SafeMarkup::checkPlain($entry[22]),
					   SafeMarkup::checkPlain($entry[23]),
					   SafeMarkup::checkPlain($entry[26]),
					   SafeMarkup::checkPlain($entry[27]),
					   SafeMarkup::checkPlain($date));
	}
      drupal_set_message(t('Data %date has been fetched.', array('%date' => $date)));
    }

    $form_state->setRedirect('get_data_content');
    return;
  }

}