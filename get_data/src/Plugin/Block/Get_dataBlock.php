<?php

namespace Drupal\get_data\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Get Data' Block
 *
 * @Block(
 *   id = "get_data_block",
 *   admin_label = @Translation("Get Data block"),
 * )
 */
class Get_dataBlock extends BlockBase implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */
/**
 * {@inheritdoc}
 */


  /**
   * {@inheritdoc}
   */
public function build() {
    return array(
      '#markup' => $this->t('Hello, World!'),
    );
  }
  /**
   * {@inheritdoc}
   */

  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('name', $form_state->getValue('hello_block_name'));
  }
  

  /**
   * {@inheritdoc}
   */

  public function defaultConfiguration() {
    $default_config = \Drupal::config('get_data.settings');
    return array(
      'name' => $default_config->get('hello.name')
    );
  }
}
?>