<?php
/**
@file
Contains \Drupal\get_data\Controller\Get_dataController.
 */

namespace Drupal\get_data\Controller;

use Drupal\get_data\Get_dataStorage;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

class Get_dataController extends ControllerBase {


  function content() {
    /*$url = Url::fromRoute('bd_contact_add');
    $add_link = ;
    $add_link = '<p>' . \Drupal::l(t('New message'), $url) . '</p>';
    $text = array(
      '#type' => 'markup',
      '#markup' => $add_link,
    );*/

    // Table header.
    $header = array(
      array (
      'data' => t('Name'),
      'field' => 'name',
      'sort' => 'asc'	
      ),
      array (
      'data' => t('Size'),
      'field' => 'size',	
      ),
      array (
      'data' => t('Price'),
      'field' => 'price',	
      ),    
      array (
      'data' => t('Type'),
      'field' => 'type',	
      ),
      array (
      'data' => t('Country'),
      'field' => 'country',	
      ),
      array (
      'data' => t('Year'),
      'field' => 'year',	
      ),
      array (
      'data' => t('Package'),
      'field' => 'package',	
      ),
      array (
      'data' => t('Alcohol %'),
      'field' => 'alcpercent',	
      ),
      array (
      'data' => t('Product Info'),
      ),
    );

    $rows = array();
    $type = 'punaviinit';

    $hdr = tablesort_get_order($header);
    $order = tablesort_get_sort($header);

    // foreach (Get_dataStorage::getType($type, $hdr['sql'], $order) as $id => $content) {
    foreach (Get_dataStorage::getAll($hdr['sql'], $order) as $id => $content) {
	$url = Url::fromRoute('get_data.content_item', array('number' => $content->number));
      // Row with attributes on the row and some of its cells.
       $rows[] = array(
        'data' => array(
         	$content->name, 
		t('@size l.', array('@size' => $content->size)), 
		t('@price euros', array('@price' => $content->price)), 
		$content->type, 
		$content->country, 
		$content->year, 
		$content->package, 
		$content->alcpercent,
		Link::fromTextAndUrl(t('More'), $url) 
        ),
      );
    }


    $table = array(
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => array(
        'id' => 'bd-contact-table',
      ),
    );

    //return $add_link . ($table);
    return array(
      //$text,
      $table,
    );
  }
}