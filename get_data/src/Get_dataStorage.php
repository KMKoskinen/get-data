<?php

namespace Drupal\get_data;

class Get_dataStorage {

  static function getAll($header, $order) {
	$query = \Drupal::database()->select('get_data', 'gd')->extend('\Drupal\Core\Database\Query\TableSortExtender');
	$query->fields('gd');
	return $query->orderBy($header, $order)->execute();
	// return $query->execute();
  }

  static function exists($id) {
    $result = db_query('SELECT 1 FROM {get_data} WHERE id = :id', array(':id' => $id))->fetchField();
    return (bool) $result;
  }

  static function getType($type, $header, $order) {
	$query = \Drupal::database()->select('get_data', 'gd')->extend('\Drupal\Core\Database\Query\TableSortExtender');
	$query->fields('gd', ['number', 'size', 'name', 'price', 'type', 'country', 'year', 'package', 'alcpercent']);
	$query->condition('gd.type', $type);
	return $query->orderBy($header, $order)->execute();
  }


  static function add($number, $name, $producer, $size, $price, $litreprice, $new, $pricingcode, $type, $grouping, $beertype, $country, $region, $year, $notes, $grapes, $descr, $package, $cork, $alcpercent, $acids, $sugar, $gravity, $kcal, $sel, $adddate) {
    db_insert('get_data')->fields(array(
	'number' => $number,
	'name' => $name,
	'producer' => $producer,
	'size' => empty(str_replace(',', '.', $size)) ? 0 : str_replace(',', '.', $size),
	'price' => str_replace(',', '.', $price),
	'litreprice' => empty(str_replace(',', '.', $litreprice)) ? 0 : str_replace(',', '.', $litreprice),
	'new' => $new,
	'pricingcode' => $pricingcode,
	'type' => $type,
	'grouping' => $grouping,
	'beertype' => $beertype,
	'country' => $country,
	'region' => $region,
	'year' => empty(str_replace(',', '.', $year)) ? NULL : str_replace(',', '.', $year),
	'notes' => $notes,
	'grapes' => $grapes,
	'descr' => $descr,
	'package' => $package,
	'cork' => $cork,
	'alcpercent' => empty(str_replace(',', '.', $alcpercent)) ? 0 : str_replace(',', '.', $alcpercent),
	'acids' => empty(str_replace(',', '.', $acids)) ? 0 : str_replace(',', '.', $acids),
	'sugar' => empty(str_replace(',', '.', $sugar)) ? 0 : str_replace(',', '.', $sugar),
	'gravity' => empty(str_replace(',', '.', $gravity)) ? 0 : str_replace(',', '.', $gravity),
	'kcal' => empty(str_replace(',', '.', $kcal)) ? NULL : str_replace(',', '.', $kcal),
	'sel' => $sel,
	'adddate' => $adddate
	))->execute(); 
  }

  static function delete($id) {
    db_delete('get_data')->condition('id', $id)->execute();
  }

}